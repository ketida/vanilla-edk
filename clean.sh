#!/bin/bash
rm -rf ./editoria/packages/*/node_modules
rm -rf ./editoria/node_modules
rm -rf ./editoria-vanilla/node_modules
rm -rf ./wax/packages/*/node_modules
rm -rf ./wax/node_modules
rm -rf ./node_modules
